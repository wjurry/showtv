
@extends('frontend.main')

@section('title', 'Show')

@section('csrf-token-meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('contents')
    <div class="row mt-5">
        <div class="col-md-8 mb-5">
            <h2>{{ $show->title }}</h2>
            <hr>
            <p>{{ $show->description }}</p>
            <div class="row">
                @foreach($show->episodes as $episode)
                <div class="col-sm-6">
                    <div class="card">
                        <img class="card-img-top" src="/{{ $episode->thumbnail }}" alt="">
                        <div class="card-body">
                            <h5 class="card-title">{{ $episode->title }}</h5>
                            <p class="card-text">{{ $episode->description }}</p>
                            <a href="{{ url(sprintf('show/%d/episode/%d', $show->id, $episode->id)) }}" class="btn btn-primary">View Episode</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @if(Auth::check())
        <div class="col-md-4 mb-5" id="actions">
            @if(!$isFollowed)
            <h2>Follow this show</h2>
            <a class="btn btn-success follow" href="{{ url('follow') }}" data-id="{{ $show->id }}" data-next-url="{{ url('unfollow') }}">Follow</a>
            @else
            <h2>Un Follow this show</h2>
            <a class="btn btn-primary unfollow" href="{{ url('unfollow') }}" data-id="{{ $show->id }}" data-next-url="{{ url('follow') }}">Un Follow</a>
            @endif
        </div>
        @endif
    </div>
    <!-- /.row -->

@endsection

@section('extraScripts')
    <script type="text/javascript" src="/{{ asset('js/follow.js') }}"></script>
@endsection
