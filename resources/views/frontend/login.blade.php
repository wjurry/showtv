@extends('frontend.main')

@section('title', 'Login')

@section('contents')
<div id="logreg-forms">
    @include('flash-messages')
    @yield('content')
    <form class="form-signin" method="post" action="{{ url('login') }}">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Sign in</h1>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">

        <button class="btn btn-success btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> Sign in</button>
        <hr>
        <p style="text-align:center">OR</p>
        <button class="btn btn-primary btn-block" type="button" id="btn-signup"><i class="fas fa-user-plus"></i> Sign up New Account</button>
    </form>

    <form action="{{ url('register') }}" class="form-signup" method="post">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Register</h1>
        <input type="text" name="full_name" id="user-name" class="form-control" placeholder="Full name" required="" autofocus="">
        <input type="email" name="email" id="user-email" class="form-control" placeholder="Email address" required autofocus="">
        <input type="password" name="password" id="user-pass" class="form-control" placeholder="Password" required autofocus="">
        <input type="password" name="retype_password" id="user-repeatpass" class="form-control" placeholder="Repeat Password" required autofocus="">

        <button class="btn btn-primary btn-block" type="submit"><i class="fas fa-user-plus"></i> Sign Up</button>
        <a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Back</a>
    </form>
    <br>

</div>
@endsection
