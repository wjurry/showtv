@extends('frontend.main')

@section('contents')
    <div class="col-md-12 mt-5 mb-5">
        <h2>Search results for: "{{ $searchFor }}"</h2>
        <hr />

        @if(!$results->count())
            <h4>No results found!</h4>
            @endif

        @if($searchIn == 'episodes')
            @foreach($results as $episode)
                <div class="col-sm-12">
                    <div class="row p-2">
                        <div class="col-md-3">
                            <img class="card-img" src="/{{ $episode->thumbnail }}" alt="" style="max-width: 200px;">
                        </div>
                        <div class="col-md-8">
                            <h5 class="card-title">{{ $episode->title }}</h5>
                            <p class="card-text">{{ $episode->description }}</p>
                            <a href="{{ url(sprintf('show/%d/episode/%d', $episode->tvShow->id, $episode->id)) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
                @endforeach
        @else
            @foreach($results as $show)
                <div class="col-sm-12">
                    <div class="row p-2">
                        <div class="col-md-3">
                            <img class="card-img" src="/{{ asset('images') . '/' . $show->id . '.jpg' }}" style="max-width: 200px;">
                        </div>
                        <div class="col-md-8">
                            <h5 class="card-title">{{ $show->title }}</h5>
                            <p class="card-text">{{ $show->description }}</p>
                            <a href="{{ url(sprintf('show/%d', $show->id)) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
                @endforeach
        @endif
    </div>

    <div class="col-md-12 d-flex">
        <div class="mx-auto">
            {{ $results->appends(Request::except('page'))->links("pagination::bootstrap-4") }}
        </div>
    </div>
@endsection
