
@extends('frontend.main')

@section('title', 'Episode')

@section('csrf-token-meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('contents')
<div class="row mt-5">
    <div class="col-md-8 mb-5">
        <img src="/{{ $episode->thumbnail }}" />
        <h2>{{ $episode->tvShow->title . ' - ' . $episode->title}}</h2>
        <hr>
        <p>{{ $episode->description }}</p>
        <iframe width="560" height="315" src="{{ $episode->video_content }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="col-md-4 mb-5">
        <div class="col-md-12" id="actions">
            @if(!$isLiked)
                <h2>Do you like it ?</h2>
                <a class="btn btn-primary like" href="{{ url('like') }}" data-id="{{ $episode->id }}" data-next-url="{{ url('unlike') }}">Like</a>
            @else
                <h2>Unlike this episode</h2>
                <a class="btn btn-warning unlike" href="{{ url('unlike') }}" data-id="{{ $episode->id }}" data-next-url="{{ url('like') }}">Un Like</a>
            @endif
        </div>
        <hr />
        <h2>Back to main show</h2>
        <a href="{{ url('show/'.$episode->tvShow->id) }}">Go Back!</a>
    </div>
</div>
<!-- /.row -->

@endsection

@section('extraScripts')
    <script type="text/javascript" src="/{{ asset('js/like.js') }}"></script>
@endsection
