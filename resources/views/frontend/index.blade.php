
@extends('frontend.main')

@section('title', 'Home Page')

@section('header')
<header class="bg-primary py-5 mb-5">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-lg-12">
                @if(!count($episodes))
                <div class="row text-center">
                    <div class="col-lg-12">
                        <h1>No Tv Shows Found!</h1>
                    </div>
                </div>
                @else
                <div class="row text-center">
                    <div class="col-lg-12">
                        <h1>Latest Episodes</h1>
                    </div>
                </div>
                <div class="row">
                    @foreach($episodes as $episode)
                    <div class="col-md-3 mb-5">
                        <div class="card h-100">
                            <img class="card-img-top" src="{{ $episode->thumbnail }}" alt="">
                            <div class="card-body">
                                <h4 class="card-title">{{ $episode->tvShow->title . ' - ' . $episode->title }}</h4>
                                <p class="card-text">{{ $episode->description }}</p>
                            </div>
                            <div class="card-footer">
                                <a href="{{ url('show/' . $episode->tvShow->id) }}" class="btn btn-primary">Find Out More!</a>
                            </div>
                        </div>
                    </div>
                        @endforeach
                </div>
                @endif
                <!-- /.row -->
            </div>
        </div>
    </div>
</header>
@endsection

@section('contents')
<div class="row">
    <div class="col-md-8 mb-5">
        <h2>Technical Test</h2>
        <hr>
        <p>This is a demo website for the technical test</p>
        <a class="btn btn-primary btn-lg" href="#">Admin area &raquo;</a>
    </div>
    <div class="col-md-4 mb-5">
        <h2>Contact Us</h2>
        <hr>
        <address>
            <strong>Eng. Wajdi Jurry</strong>
            <br>11121 Amman
            <br>Amman, Jordan
            <br>
        </address>
        <address>
            <abbr title="Phone">P:</abbr>
            (962) 79-584-5634
            <br>
            <abbr title="Email">E:</abbr>
            <a href="mailto:jurrywajdi@yahoo.com">jurrywajdi@yahoo.com</a>
        </address>
    </div>
</div>
<!-- /.row -->

@endsection
