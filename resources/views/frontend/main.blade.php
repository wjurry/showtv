<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    @yield('csrf-token-meta')

    <title>Show TV! - @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="/{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="/{{ asset('css/custom.css') }}" rel="stylesheet">
</head>

<body>

<!-- Navigation Menu -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">{{ env('APP_TITLE') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @if(!Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('login') }}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('login') }}">Register</a>
                </li>
                @else
                <li class="nav-item">
                    <span class="d-inline-block" style="color: #fff;"><i>Hi {{ Auth::user()->full_name }} |</i><a class="nav-link d-inline-block" href="{{ url('logout') }}">Logout</a></span>
                </li>
                @endif
            </ul>
            <form class="form-inline my-2 my-lg-0" action="{{ url('search') }}">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <select name="search_in">
                            <option value="shows">Shows</option>
                            <option value="episodes">Episodes</option>
                        </select>
                    </div>
                    <input class="form-control mr-sm-2" name="search_for" type="search" placeholder="Search" aria-label="Search" value="{{ $searchFor ?? '' }}">
                </div>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>

<!-- Header -->
@yield('header')

<!-- Page Content -->
<div class="container">

    @yield('contents')

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; 2019</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
@section('scripts')
<script src="/{{ asset('js/jquery.min.js') }}"></script>
<script src="/{{ asset('js/jquery.cookie.js') }}"></script>
<script src="/{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="/{{ asset('js/custom.js') }}"></script>

    @yield('extraScripts')
@endsection

@yield('scripts')

</body>

</html>
