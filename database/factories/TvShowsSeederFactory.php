<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\TvShow::class, function (Faker $faker, array $extraData = []) use($factory) {
    return array_merge($extraData, [
        'title' => $faker->text(50),
        'description' => $faker->text(100)
    ]);
});
