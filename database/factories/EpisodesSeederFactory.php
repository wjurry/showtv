<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

function generator() {
    for ($i = 1; $i <= 10; ++$i)
        yield $i;
}

$number = generator();

$factory->define(\App\Episode::class, function (Faker $faker, array $extraData) use ($factory, &$number) {
    $faker->addProvider(new \Faker\Provider\Youtube($faker));
    if (is_null($number->current())) {
        $number = generator();
    }
    $data = [
        'title' => sprintf('Episode %d', $number->current()),
        'description' => $faker->text(200),
        'thumbnail' => asset('images') . sprintf('/%d.jpg', $extraData['show_id']),
        'video_content' => $faker->youtubeUri()
    ];
    $number->next();
    return $data;
});
