<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFollowsTable extends Migration
{
    const TABLE_NAME = 'follows';
    const USER_FOREIGN_KEY_NAME = 'follows_user_id_fk';
    const TV_SHOWS_FOREIGN_KEY_NAME = 'follows_show_id_fk';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follows', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('show_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->dateTime('followed_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            // Add unique index
            $table->unique(['show_id', 'user_id']);

            // Add foreign keys
            $table->foreign('user_id', self::USER_FOREIGN_KEY_NAME)
                ->references('id')->on('users');
            $table->foreign('show_id', self::TV_SHOWS_FOREIGN_KEY_NAME)
                ->references('id')->on('tv_shows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign keys
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign(self::USER_FOREIGN_KEY_NAME);
            $table->dropForeign(self::TV_SHOWS_FOREIGN_KEY_NAME);
        });

        // Drop table
        Schema::dropIfExists('follows');
    }
}
