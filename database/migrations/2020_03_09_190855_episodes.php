<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Episodes extends Migration
{
    const TABLE_NAME = 'episodes';
    const FOREIGN_KEY_NAME = 'episods_tv_shows_id_fk';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->bigInteger('show_id', false, true);
            $table->string('title', 100);
            $table->text('description')->nullable(true);
            $table->text('thumbnail')->nullable(true);
            $table->text('video_content')->nullable(true);
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            // Add foreign key
            $table->foreign('show_id', self::FOREIGN_KEY_NAME)
                ->references('id')->on('tv_shows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign key
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign(self::FOREIGN_KEY_NAME);
        });


        // Drop table
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
