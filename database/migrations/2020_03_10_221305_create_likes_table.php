<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateLikesTable extends Migration
{
    const TABLE_NAME = 'likes';
    const USER_FOREIGN_KEY_NAME = 'likes_user_id_fk';
    const EPISODES_FOREIGN_KEY_NAME = 'likes_episode_id_fk';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->bigInteger('episode_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->dateTime('liked_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            // Add unique index
            $table->unique(['episode_id', 'user_id']);

            // Add foreign keys
            $table->foreign('episode_id', self::USER_FOREIGN_KEY_NAME)
                ->references('id')->on('episodes');
            $table->foreign('user_id', self::EPISODES_FOREIGN_KEY_NAME)
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign keys
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign(self::USER_FOREIGN_KEY_NAME);
            $table->dropForeign(self::EPISODES_FOREIGN_KEY_NAME);
        });

        // Drop table
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
