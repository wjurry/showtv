<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Users extends Migration
{
    const TABLE_NAME = 'users';
    const FOREIGN_KEY_NAME = 'users_roles_id_fk';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->bigInteger('role_id', false, true);
            $table->string('email', 100)->unique();
            $table->string('full_name', 100);
            $table->text('password');
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            // Add foreign key
            $table->foreign('role_id', self::FOREIGN_KEY_NAME)
                ->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign key
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign(self::FOREIGN_KEY_NAME);
        });

        // Drop table
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
