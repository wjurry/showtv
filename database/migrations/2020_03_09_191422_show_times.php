<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ShowTimes extends Migration
{
    const TABLE_NAME = 'show_times';
    const FOREIGN_KEY_TV_SHOWS_NAME = 'show_times_tv_shows_id_fk';
    const FOREIGN_KEY_EPISODES_NAME = 'show_times_episodes_id_fk';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->bigInteger('show_id', false, true);
            $table->dateTime('show_time');

            // Add foreign key
            $table->foreign('show_id', self::FOREIGN_KEY_TV_SHOWS_NAME)
                ->references('id')->on('tv_shows');
            $table->foreign('show_id', self::FOREIGN_KEY_EPISODES_NAME)
                ->references('id')->on('episodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign key
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign(self::FOREIGN_KEY_TV_SHOWS_NAME);
        });

        // Drop table
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
