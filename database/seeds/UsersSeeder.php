<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = \App\Role::where('title', 'admin')->first();
        $roleUser = \App\Role::where('title', 'user')->first();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();

        // truncate table before start
        DB::table('users')->insert([
            'full_name' => 'Admin User',
            'role_id' => $roleAdmin->id,
            'email' => 'admin@localhost.com',
            'password' => Hash::make('admin'),
            'created_at' => date('Y-m-d', time())
        ]);

        DB::table('users')->insert([
            'full_name' => 'Normal User',
            'role_id' => $roleUser->id,
            'email' => 'normal@localhost.com',
            'password' => Hash::make('normal'),
            'created_at' => date('Y-m-d', time())
        ]);
    }
}
