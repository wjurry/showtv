<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TvShowsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // truncate tables
        // truncate table before start
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('tv_shows')->truncate();
        DB::table('episodes')->truncate();

        $adminRole = \App\Role::where(['title' => 'admin'])->first();
        $adminUser = \App\User::where(['role_id' => $adminRole->id])->first();

        factory(App\TvShow::class, 10)->create([
            'user_id' => $adminUser->id
        ])->each(function (\App\TvShow $tvShow) {
            $tvShow->episodes()->saveMany(
                factory(\App\Episode::class, 10)->make([
                    'show_id' => $tvShow->id
                ])
            );
        });
    }
}
