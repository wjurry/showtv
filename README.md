# Technical Test

## Introduction
- This is the source code of the technical test requested by Roya.tv
- Application name: Show TV
- Developer info:
  - Name: Wajdi Jurry
  - Mobile number: +962-79-584-5634
  - E-mail: [jurrywajdi@yahoo.com](mailto:jurrywajdi@yahoo.com)
  

- App development environment:
  - PHP 7.3
  - Laravel Framework 7.0.8
  - Apache2 web server
  - MySQL 8
  
## Installation
- Clone this repository:
```shell script
# git clone git@gitlab.com:wjurry/showtv.git
```

- Go inside project folder:
```shell script
# cd showtv
```

- Install dependencies
```shell script
/path/to/showtv# php composer.phar install
```

- Rename ```.env.example``` to ```.env``` and edit it to match your configuration

- Create new schema and name it whatever you want, but remember to reflect this on ```.env``` file

- Run migrations to create tables and indexes:
```shell script
/path/to/showtv# php artisan migrate:fresh
```

- Run seeds to populate data:
```shell script
/path/to/showtv# php artisan db:seed
```
This will create two users with specific roles, normal and admin, also will create dummy data for shows and episodes

## Using the app
- Start a PHP server, by running this command:
```shell script
/path/to/showtv# php artisan serve
Laravel development server started: http://127.0.0.1:8000
```
- If you run the seeds, then you can login by either of these two users:
  - Normal user:
     > - E-mail: normal@localhost.com
     > - Password: normal

  - Admin user:
     > - E-mail: admin@localhost.com
     > - Password: admin
