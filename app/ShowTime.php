<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowTime extends Model
{
    public function tvShow()
    {
        return $this->belongsTo('App\TvShow', 'show_id');
    }

    public function episode()
    {
        return $this->belongsTo('App\Episode', 'show_id');
    }
}
