<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    const UPDATED_AT = null;

    protected $dates = ['created_at'];

    public function tvShow()
    {
        return $this->belongsTo('App\TvShow', 'show_id', 'id');
    }

    public function likes()
    {
        return $this->hasMany('App\Like', 'episode_id', 'id');
    }
}
