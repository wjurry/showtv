<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    const CREATED_AT = 'followed_at';
    const UPDATED_AT = null;

    public $fillable = ['show_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function episode()
    {
        return $this->belongsTo('App\Episode', 'episode_id', 'id');
    }
}
