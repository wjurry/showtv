<?php


namespace App\Http\Controllers;



use App\Episode;
use App\Follow;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Like;
use App\TvShow;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class FrontendController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // get 4 random episodes
        $episodes = Episode::all();
        if ($episodes->count() > 4) {
            $episodes = Episode::all()->random(4);
        }
        return view('frontend/index', ['episodes' => $episodes]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function login()
    {
        if (Auth::check()) {
            return redirect('/');
        }
        return view('frontend/login');
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doLogin(LoginRequest $request)
    {
        if (!Auth::attempt([
            'email' => $request->post('email'),
            'password' => $request->post('password')
        ])) {
            return back()->with('warning', 'Invalid username or password');
        }

        return redirect('/');
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doRegister(RegisterRequest $request)
    {
        // check if email exists
        if (User::where('email', $request->post('email'))->exists()) {
            return back()->with('warning', 'This email is already exists');
        }

        // register user
        $user = User::create([
            'full_name' => $request->post('full_name'),
            'email' => $request->post('email'),
            'password' => Hash::make($request->post('password'))
        ]);

        // login user
        Auth::login($user);

        return redirect('/');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doLogout()
    {
        if (Auth::check()) {
            Auth::logout();
        }
        return redirect('/');
    }

    public function viewTvShow($id)
    {
        if (!is_numeric($id)) {
            return redirect('/');
        }

        $shows = TvShow::where(['id' => $id]);
        if (!$shows->count()) {
            return redirect('/');
        }

        $show = $shows->first();

        $isFollowed = false;
        if (Auth::check()) {
            if ($show->follows()->where(['user_id' => Auth::user()->id, 'show_id' => $id])->count()) {
                $isFollowed = true;
            }
        }

        return view('frontend/show', ['show' => $show, 'isFollowed' => $isFollowed]);
    }

    public function viewEpisode($showId, $episodeId)
    {
        if (empty($episodeId)) {
            return redirect('/');
        }
        $episodes = Episode::where(['id' => $episodeId, 'show_id' => $showId]);
        if (!$episodes->count()) {
            return redirect('/');
        }

        $episode = $episodes->first();

        $isLiked = false;
        if ($episode->likes()->where(['episode_id' => $episodeId, 'user_id' => Auth::user()->id])->count()) {
            $isLiked = true;
        }
        return view('frontend/episode', ['episode' => $episode, 'isLiked' => $isLiked]);
    }

    public function doFollow(Request $request)
    {
        $showId = $request->post('showId');
        Follow::create([
            'show_id' => $showId,
            'user_id' => Auth::user()->id
        ]);

        return true;
    }

    public function doUnFollow(Request $request)
    {
        $showId = $request->post('showId');
        Follow::where([
            'show_id' => $showId,
            'user_id' => Auth::user()->id
        ])->delete();

        return true;
    }

    public function doLike(Request $request)
    {
        $episodeId = $request->post('episodeId');
        Like::create([
            'episode_id' => $episodeId,
            'user_id' => Auth::user()->id
        ]);

        return true;
    }

    public function doUnLike(Request $request)
    {
        $episodeId = $request->post('episodeId');
        Like::where([
            'episode_id' => $episodeId,
            'user_id' => Auth::user()->id
        ])->delete();

        return true;
    }

    public function search(Request $request)
    {
        $searchFor = $request->get('search_for');
        $searchIn = $request->get('search_in');

        if ($searchIn == 'shows') {
            $results = TvShow::where(function ($query) use ($searchFor) {
                $query->where('title', 'like', '%' . $searchFor . '%');
                $query->orWhere('description', 'like', '%' . $searchFor . '%');
            });
        } else {
            $results = Episode::where(function ($query) use ($searchFor) {
                $query->where('title', 'like', '%' . $searchFor . '%');
                $query->orWhere('description', 'like', '%' . $searchFor . '%');
            });
        }

        $results = collect($results->get())->paginate(5);

        return view('frontend/search', ['results' => $results, 'searchIn' => $searchIn, 'searchFor' => $searchFor]);
    }
}
