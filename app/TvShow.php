<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TvShow extends Model
{
    const UPDATED_AT = null;

    protected $dates = ['created_at'];

    public function episodes()
    {
        return $this->hasMany('App\Episode', 'show_id', 'id');
    }

    public function follows()
    {
        return $this->hasMany('App\Follow', 'show_id', 'id');
    }
}
