<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');
Route::get('/login', 'FrontendController@login');
Route::post('/login', 'FrontendController@doLogin');
Route::post('/register', 'FrontendController@doRegister');
Route::get('/logout', 'FrontendController@doLogout');
Route::get('/show/{showId}', 'FrontendController@viewTvShow');
Route::get('/show/{showId}/episode/{episodeId}', 'FrontendController@viewEpisode');
Route::get('/search', 'FrontendController@search');

Route::post('/follow', [
    'middleware' => 'auth',
    'uses' => 'FrontendController@doFollow'
]);
Route::post('/unfollow', [
    'middleware' => 'auth',
    'uses' => 'FrontendController@doUnFollow'
]);
Route::post('/like', [
    'middleware' => 'auth',
    'uses' => 'FrontendController@doLike'
]);
Route::post('/unlike', [
    'middleware' => 'auth',
    'uses' => 'FrontendController@doUnLike'
]);
