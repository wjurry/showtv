$(() => {
    $(document).on('click', '.like', function (e) {
        e.preventDefault();
        var episodeId = $(this).data('id');
        likeUnlike($(this), episodeId);
    });

    $(document).on('click', '.unlike', function (e) {
        e.preventDefault();
        var episodeId = $(this).data('id');
        likeUnlike($(this), episodeId);
    });
});

function likeUnlike(element, episodeId) {
    var isLiked = !!element.hasClass('like');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: element.attr('href'),
        type: 'POST',
        data: 'episodeId='+episodeId,
        success: function() {
            element.remove();
            $('#actions').append(
                $('<a></a>')
                    .attr('href', element.data('next-url'))
                    .attr('data-next-url', element.attr('href'))
                    .addClass('btn btn-' + (isLiked ? 'warning' : 'primary'))
                    .addClass(isLiked ? 'unlike' : 'like')
                    .attr('data-id', episodeId)
                    .text((isLiked ? 'Un Like' : 'Like'))
            );
        }
    });
}
