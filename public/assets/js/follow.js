$(() => {
    $(document).on('click', '.follow', function(e) {
        e.preventDefault();
        var showId = $(this).data('id');
        followUnfollow($(this), showId);
    });

    $(document).on('click', '.unfollow', function (e) {
        e.preventDefault();
        var showId = $(this).data('id');
        followUnfollow($(this), showId);
    });
});

function followUnfollow(element, showId) {
    var isFollow = !!element.hasClass('follow');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: element.attr('href'),
        type: 'POST',
        data: 'showId='+showId,
        success: function() {
            element.remove();
            $('#actions').append(
                $('<a></a>')
                    .attr('href', element.data('next-url'))
                    .attr('data-next-url', element.attr('href'))
                    .addClass('btn btn-' + (isFollow ? 'primary' : 'success'))
                    .addClass(isFollow ? 'unfollow' : 'follow')
                    .attr('data-id', showId)
                    .text((isFollow ? 'Un Follow' : 'Follow'))
            );
        }
    });
}
