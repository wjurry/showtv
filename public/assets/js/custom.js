function toggleSignUp(e){
    e.preventDefault();
    $('#logreg-forms .form-signin').hide(); // display:block or none
    $('#logreg-forms .form-signup').show(); // display:block or none
    $.cookie('form', $(e.currentTarget).attr('id') === 'btn-signup' ? 'signup' : 'login');
}

function toggleSignIn(e){
    e.preventDefault();
    $('#logreg-forms .form-signin').show(); // display:block or none
    $('#logreg-forms .form-signup').hide(); // display:block or none
    $.cookie('form', $(e.currentTarget).attr('id') === 'btn-signup' ? 'signup' : 'login');
}

$(()=>{
    var signup_button = $('#logreg-forms #btn-signup');
    var cancel_signup_button = $('#logreg-forms #cancel_signup');
    setTimeout(function () {
        if ($.cookie('form') === 'signup') {
            signup_button.trigger('click');
        } else {
            cancel_signup_button.trigger('click');
        }
    }, 100);
    // Login Register Form
    signup_button.click(toggleSignUp);
    cancel_signup_button.click(toggleSignIn);
});
